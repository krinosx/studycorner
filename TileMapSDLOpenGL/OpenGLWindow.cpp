#include "OpenGLWindow.h"
#include <gl\gl.h>
#include <iostream>
#include <vector>
#define GLM_FORCE_RADIANS
#include <glm.hpp>
#include <gtc\matrix_transform.hpp> 
#include <gtx\transform.hpp>
#include <sdl.h>


void checkSDLError(int line = -1)
{
	#ifndef NDEBUG
		const char *error = SDL_GetError();
		if (*error != '\0')
		{
			printf("SDL Error: %s\n", error);
			if (line != -1) {
					printf(" + line: %i\n", line);
			}
			SDL_ClearError();
		}
	#endif
}

void donothing()
{





}

/**
 * Class constructor 
 */
OpenGLWindow::OpenGLWindow(const char * title, int width, int height)
{
	this->windowTitle = title;
	this->windowWidth = width;
	this->windowHeight = height;

	if ( SDL_Init( SDL_INIT_EVERYTHING ) != 0 )
	{
		checkSDLError( __LINE__ );
		exit(-1);
	}
	
    /* Request opengl 3.2 context. */
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 2 );
 
    /* Turn on double buffering with a 24bit Z buffer.
     * You may need to change this to 16 or 32 for your system */
    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
    SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 24 );
 	
	this->sdlWindow = SDL_CreateWindow( this->windowTitle, 50 , 50 , this->windowWidth , this->windowHeight , SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );

	if ( this->sdlWindow == NULL )
	{
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
	}
	
    this->openglContext = SDL_GL_CreateContext( this->sdlWindow );
	if( this->openglContext == NULL ) 
	{
		checkSDLError( __LINE__ );
		exit(-1);
	}

	if( SDL_GL_MakeCurrent( this->sdlWindow, this->openglContext ) < 0 )
	{
		checkSDLError( __LINE__ );
		exit(-1);
	}
	
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if( GLEW_OK != err )
	{
		std::cout << "Error starting glew";
		exit(-1);
	}

	
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS); 

}

/**
 * Class destructor
 */
OpenGLWindow::~OpenGLWindow()
{
	// Restore the cursor to its previous area. 
	SDL_GL_DeleteContext(this->openglContext);
	SDL_DestroyWindow(this->sdlWindow);
	SDL_Quit();
}

/*
 * Private function responsible to draw stuff  
 */
void OpenGLWindow::draw( )
{
		glClearColor( 0.0, 0.0, 0.0, 1.0 );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		int qtVos = (int) this->objects.size();
		
		for( int i = 0; i < qtVos; i++ )
		{
			this->objects[i].draw( this->camera );
		}
		
		SDL_GL_SwapWindow( this->sdlWindow );
}

/*
 * public function to start the render/event loop 
 */
void OpenGLWindow::startLoop( )
{
	this->running = true;

	SDL_Event sdlEvent;
	while( this->isRunning() )
	{
		while ( SDL_PollEvent(&sdlEvent) )
		{	
			//If user closes the window
			if (sdlEvent.type == SDL_QUIT)
			{
				this->running = false;
			}
			//If user presses any key
			if (sdlEvent.type == SDL_KEYDOWN) 
			{
				this->keyListener->keyPressed( sdlEvent.key.keysym.sym );
			}
			//If user clicks the mouse
			if ( sdlEvent.type == SDL_MOUSEBUTTONDOWN )
			{
				this->keyListener->mousePressed( sdlEvent.button.x, sdlEvent.button.y );
			}
			this->draw();
		}
	}
}


void OpenGLWindow::addObject( VisualObject obj )
{
	this->objects.push_back( obj );	
}

bool OpenGLWindow::isRunning()
{
	return this->running;
}

void OpenGLWindow::setCamera( Camera myCamera )
{
	this->camera = myCamera;
}


void OpenGLWindow::setListener( InputHandler *keyListener )
{
	this->keyListener = keyListener;
}

/*
 * Inherited methods from InputHandler interface
 *  -- Event handling -- 
 * 
 */ 
void OpenGLWindow::keyPressed(SDL_Keycode key)
{
	switch( key )
	{
		case SDLK_ESCAPE:
			this->running = false;
			break;
		case SDLK_UP:
			this->camera.moveCamera(0.0f,0.0f,0.5f);
			break;
		case SDLK_DOWN:
			this->camera.moveCamera(0.0f,0.0f,-0.5f);
			break;
		case SDLK_w:
			this->camera.moveCamera( 0.05f, 0.0f, 0.0f );
			break;
		case SDLK_s:
			this->camera.moveCamera( -0.05f, 0.0f, 0.0f );
			break;
		case SDLK_a:
			this->camera.rotateCamera( 0.05f, 0.0f );
			break;
		case SDLK_d:
			this->camera.rotateCamera( -0.05f, 0.0f );
			break;
		case SDLK_q:
			this->camera.rotateCamera(0.05f,0.0f);				
			break;
		case SDLK_e:
			this->camera.rotateCamera(-0.05f,0.0f);
			break;
		case SDLK_r:
			this->camera.rotateCamera(0.0f,0.05f);
			break;
		case SDLK_f:
			this->camera.rotateCamera(0.0f,-0.05f);
			break;
		case SDLK_z:
			this->camera.moveCamera(0.0f,0.5f,0.0f);
			break;
		case SDLK_x:
			this->camera.moveCamera(0.0f,-0.5f,0.0f);
			break;
		default:
			break;
	}
}

void OpenGLWindow::mousePressed( int x, int y )
{
	std::cout << "Mouse Press on X=" << x << " Y=" << y << "\r\n";
}


