#ifndef TEXTURE_H
#define TEXTURE_H
#include <GL/glew.h>
class Texture
{
private:
	GLuint textureID;
public:
	Texture( const char * imagePath  );
	~Texture();
	GLuint getTextureId();
};

#endif // TEXTURE_H
