/**
 * Handle texture loading and instancing
 *
 *
 * TODO list
 *  - remove the custom code 'loadBMP_custom' and use SDL code to load images.
 *
 */

#include "texture.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>

struct IMG_DATA {
	int width;
	int height;
	int status;
	char * erroMsg;
	unsigned char * imageData;
};

typedef IMG_DATA ImageData;

ImageData loadBMP_custom(const char * imagepath){

	ImageData imgData;
	
	printf("Reading image %s\n", imagepath);

	// Data read from the header of the BMP file
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	unsigned int width, height;
	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file = fopen(imagepath,"rb");
	if (!file){ 
		printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", imagepath); 
		getchar(); 
		imgData.status = -1;
		imgData.erroMsg = "Could not open the file";
		return imgData;
	}

	// Read the header, i.e. the 54 first bytes

	// If less than 54 bytes are read, problem
	if ( fread(header, 1, 54, file)!=54 ){ 
		printf("Not a correct BMP file\n");
		imgData.status = -1;
		imgData.erroMsg = "Not a correct BMP file\n";
		return imgData;
	}
	// A BMP files always begins with "BM"
	if ( header[0]!='B' || header[1]!='M' ){
		printf("Not a correct BMP file\n");
		imgData.status = -1;
		imgData.erroMsg = "Not a correct BMP file\n";
		return imgData;
	}
	// Make sure this is a 24bpp file
	if ( *(int*)&(header[0x1E])!=0  ){
		printf("Not a correct BMP file\n");    
		imgData.status = -1;
		imgData.erroMsg = "Not a correct BMP file\n";
		return imgData;
	}
	if ( *(int*)&(header[0x1C])!=24 ) {
		printf("Not a correct BMP file\n");    
		imgData.status = -1;
		imgData.erroMsg = "Not a correct BMP file\n";
		return imgData;
	}

	// Read the information about the image
	dataPos    = *(int*)&(header[0x0A]);
	imageSize  = *(int*)&(header[0x22]);
	width      = *(int*)&(header[0x12]);
	height     = *(int*)&(header[0x16]);


	// Some BMP files are misformatted, guess missing information
	if (imageSize==0)    imageSize=width*height*3; // 3 : one byte for each Red, Green and Blue component
	if (dataPos==0)      dataPos=54; // The BMP header is done that way

	// Create a buffer
	data = new unsigned char [imageSize];

	// Read the actual data from the file into the buffer
	fread(data,1,imageSize,file);

	// Everything is in memory now, the file wan be closed
	fclose (file);

	imgData.width = width;
	imgData.height = height;
	imgData.imageData = data;
	
	return imgData;
}

Texture::Texture( const char * imagePath )
{
	ImageData data = loadBMP_custom( imagePath );
	
	// Create one OpenGL texture
	glGenTextures(1, &this->textureID);
	
	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, this->textureID);

	// Give the image to OpenGL
	glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, data.width, data.height, 0, GL_BGR, GL_UNSIGNED_BYTE, data.imageData );

	// OpenGL has now copied the data. Free our own version
	delete [] data.imageData;

	// ... nice trilinear filtering.
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); 
	glGenerateMipmap(GL_TEXTURE_2D);
}

Texture::~Texture()
{
	
}

GLuint Texture::getTextureId()
{
	return this->textureID;
}
