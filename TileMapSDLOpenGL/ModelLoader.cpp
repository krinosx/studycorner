#include "ModelLoader.h"
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <vector>

//#define _DEBUG_MODEL_LOADER_

ModelLoader::ModelLoader(void)
{
}


ModelLoader::~ModelLoader(void)
{
}


struct VERTEX_UV_NORMAL_PAIR
{
	GLfloat vertex[3];
	GLfloat uv[2];
	GLfloat normal[3];
};

int existPair( VERTEX_UV_NORMAL_PAIR pair , GLfloat * outputVertexData, GLfloat * outputUvData, GLfloat * outputNormalData, int bufferCount, int outputCount );

Model * ModelLoader::loadModel(std::string filePath)
{

	std::string line;
	GLfloat* vertexData;

	GLfloat* uvData;
	int uvDataCount = 0;

	GLfloat* normalData;
	int normalDataCount = 0;


	GLuint * vertexIndex;
	GLint * normalIndex;
	GLint * textureIndexIndex;
	int dataCount = 0;
	int indexCount = 0;

	std::ifstream modelFile;
	modelFile.open( filePath.c_str() );

	modelFile.seekg( 0, modelFile.end );
	// File Size
	int fileSize = (int) modelFile.tellg();
	modelFile.seekg( 0, modelFile.beg );

	
	vertexData = (GLfloat *) malloc( fileSize );

	uvData = (GLfloat *) malloc( fileSize );

	normalData = (GLfloat *) malloc( fileSize );

	vertexIndex = (GLuint *) malloc( fileSize );

	textureIndexIndex = (GLint *) malloc( fileSize );

	normalIndex = (GLint *) malloc( fileSize );


	while ( !modelFile.eof() )
	{
		std::getline( modelFile, line );

		if( line.c_str()[0] == 'v' && line.c_str()[1] == ' ' )
		{
			line[0] = ' ';

			sscanf(line.c_str(), "%f %f %f ",
				&vertexData[dataCount],
				&vertexData[dataCount+1],
				&vertexData[dataCount+2]);

			dataCount += 3;
		}
		else if( line.c_str()[0] == 'v' && line.c_str()[1] == 't' ) // uv data
		{

			line[0] = ' ';
			line[1] = ' ';

			sscanf(line.c_str(), "%f %f ",
				&uvData[uvDataCount],
				&uvData[uvDataCount+1]
			);
			uvDataCount += 2;

		} else if( line.c_str()[0] == 'v' && line.c_str()[1] == 'n' ) // normal data
		{
			line[0] = ' ';
			line[1] = ' ';

			sscanf(line.c_str(), "  %f %f %f ",
				&normalData[normalDataCount],
				&normalData[normalDataCount+1],
				&normalData[normalDataCount+2] );

			normalDataCount += 3;

		}
		else if( line.c_str()[0] == 'f' ) // face
		{
			line[0] = ' '; // clear the f character

			sscanf(line.c_str(), "%d/%d/%d %d/%d/%d %d/%d/%d ", 
				&vertexIndex[indexCount],
				&textureIndexIndex[indexCount],
				&normalIndex[indexCount],
				
				&vertexIndex[indexCount+1],
				&textureIndexIndex[indexCount+1],
				&normalIndex[indexCount+1],

				&vertexIndex[indexCount+2],
				&textureIndexIndex[indexCount+2],
				&normalIndex[indexCount+2]
				);


				//obj files starts with 1, our buffer starts with 0
				vertexIndex[indexCount] -= 1;
				vertexIndex[indexCount+1] -= 1;
				vertexIndex[indexCount+2] -= 1;

				textureIndexIndex[indexCount] -= 1;
				textureIndexIndex[indexCount+1] -= 1;
				textureIndexIndex[indexCount+2] -= 1;

				normalIndex[indexCount] -= 1;
				normalIndex[indexCount+1] -= 1;
				normalIndex[indexCount+2] -= 1;

				indexCount += 3;
		}
	}

	GLfloat * outputVertexData = (GLfloat * ) malloc( ( sizeof( GLfloat ) * 3 ) * indexCount );
	int outputVertexDataCount = 0;

	// Store the UV data to match the same index as vertex data
	GLfloat * outputUvData = (GLfloat *) malloc( ( sizeof( GLfloat ) * 2 ) * indexCount ); // at most 2 coords for each vertex
	int outputUvDataCount = 0;

	GLfloat * outputNormalData = (GLfloat *) malloc( ( sizeof( GLfloat ) * 3 ) * indexCount );
	int outputNormalDataCount = 0;

	// count how much elements in the output buffer
	GLuint * outputIndex = (GLuint *) malloc( ( sizeof( GLuint ) * indexCount ) );
	int outputIndexCount = 0;

	/*
	 * Reindex the buffer to be exact 1 to 1 pair with Vertex + UV + normal
	 */
	VERTEX_UV_NORMAL_PAIR pair;
	for ( int i = 0; i < indexCount; i++ )
	{
		// data position on GLfloat * vertexData
		int VERTEX_INDEX_X = ( vertexIndex[i] * 3 );
		int VERTEX_INDEX_Y = ( vertexIndex[i] * 3 ) + 1 ;
		int VERTEX_INDEX_Z = ( vertexIndex[i] * 3 ) + 2 ;

		int UV_INDEX_X = ( textureIndexIndex[i] * 2 );
		int UV_INDEX_Y = ( textureIndexIndex[i] * 2 ) + 1;

		int NORMAL_INDEX_X = ( normalIndex[i] * 3 );
		int NORMAL_INDEX_Y = ( normalIndex[i] * 3 ) + 1;
		int NORMAL_INDEX_Z = ( normalIndex[i] * 3 ) + 2;


		pair.vertex[0] = vertexData[ VERTEX_INDEX_X ];
		pair.vertex[1] = vertexData[ VERTEX_INDEX_Y ];
		pair.vertex[2] = vertexData[ VERTEX_INDEX_Z ];

		pair.uv[0] = uvData[ UV_INDEX_X ];
		pair.uv[1] = uvData[ UV_INDEX_Y ];
		
		pair.normal[0] = normalData[ NORMAL_INDEX_X ];
		pair.normal[1] = normalData[ NORMAL_INDEX_Y ];
		pair.normal[2] = normalData[ NORMAL_INDEX_Z ];


		int existsIndex = existPair(pair, outputVertexData, outputUvData, outputNormalData, indexCount, outputIndexCount );

		if( existsIndex >= 0 )
		{   // se ja existe um pair para as informacoes passadas, pega o indice retornado e adiciona 
			// no indice de saida na posicao atual
			outputIndex[i] = existsIndex;

		}
		else
		{
			// Se nao existe, entao copia as informacoes para as posicoes atuais dos bufers de saida
			
			int OUTPUT_VERTEX_INDEX_X = ( outputIndexCount * 3 );
			int OUTPUT_VERTEX_INDEX_Y = ( outputIndexCount * 3 ) + 1 ;
			int OUTPUT_VERTEX_INDEX_Z = ( outputIndexCount * 3 ) + 2 ;

			int OUTPUT_UV_INDEX_X = ( outputIndexCount * 2 );
			int OUTPUT_UV_INDEX_Y = ( outputIndexCount * 2 ) + 1;

			int OUTPUT_NORMAL_INDEX_X = ( outputIndexCount * 3 );
			int OUTPUT_NORMAL_INDEX_Y = ( outputIndexCount * 3 ) + 1;
			int OUTPUT_NORMAL_INDEX_Z = ( outputIndexCount * 3 ) + 2;


			outputVertexData[OUTPUT_VERTEX_INDEX_X] = pair.vertex[0];
			outputVertexData[OUTPUT_VERTEX_INDEX_Y] = pair.vertex[1];
			outputVertexData[OUTPUT_VERTEX_INDEX_Z] = pair.vertex[2];

			outputUvData[OUTPUT_UV_INDEX_X] = pair.uv[0];
			outputUvData[OUTPUT_UV_INDEX_Y] = pair.uv[1];

			outputNormalData[OUTPUT_NORMAL_INDEX_X] = pair.normal[0];
			outputNormalData[OUTPUT_NORMAL_INDEX_Y] = pair.normal[1];
			outputNormalData[OUTPUT_NORMAL_INDEX_Z] = pair.normal[2];

			outputIndex[i] =  outputIndexCount;
			
			outputIndexCount++;
			
			// Sum the buffer counters
			outputVertexDataCount += 3;
			outputUvDataCount += 2;
			outputNormalDataCount += 3;
			
		}
	}

	/* 
	 * Nesse momento teremos tres buffers com a mesma quantidade de indices que se correspondem
	 */


	//DEBUG
#ifdef _DEBUG_MODEL_LOADER_
	
	int debugVertexIndice = 0;
	for( int i = 0; i < outputVertexDataCount; i = i + 3 )
	{
		printf("v[%d] %f %f %f \r\n", debugVertexIndice, outputVertexData[i], outputVertexData[i+1], outputVertexData[i+2] );
		debugVertexIndice++;
	}

	int debugUvIndice = 0;
	for( int i = 0; i < outputUvDataCount; i = i+2 )
	{
		printf("u[%d] %f %f \r\n",  debugUvIndice, outputUvData[i], outputUvData[i+1] );
		debugUvIndice++;
	}

	int debugNormalIndice = 0;
	for( int i = 0; i < outputNormalDataCount; i = i + 3 )
	{
		printf("n[%d] %f %f %f \r\n", debugNormalIndice, outputNormalData[i], outputNormalData[i+1], outputNormalData[i+2] );
		debugNormalIndice++;
	}

	// print face pairs
	int debugIndexIndice = 0;
	for( int i =0; i < indexCount ; i = i + 3 )
	{
		printf("f[%d] %d %d %d \r\n", debugIndexIndice, outputIndex[i], outputIndex[i+1], outputIndex[i+2] );
		debugIndexIndice++;
	}
	// debug
#endif

	Model * model = new Model();
	
	model->setVertexData( outputVertexData, ( sizeof(GLfloat) * outputVertexDataCount ) , outputVertexDataCount );
	model->setUvData( outputUvData, ( sizeof(GLfloat) * outputUvDataCount ), outputUvDataCount );
	model->setNormalData( outputNormalData, ( sizeof( GLfloat ) * outputNormalDataCount ), outputNormalDataCount );
	model->setVertexIndexData( outputIndex, (sizeof(GLint) * indexCount), indexCount );

	free( vertexData );
	free( uvData );
	free( vertexIndex );
	free( textureIndexIndex );
	free( normalIndex );
	free( normalData );

	free( outputVertexData );
	free( outputUvData );
	free( outputNormalData );
	free( outputIndex );


	modelFile.close();
	
	return model;
}



bool is_near(GLfloat v1, GLfloat v2){
	return fabs( v1-v2 ) < 0.01f;
}

/**
* Chek if VERTEX_UV_PAIR exists and return it index, or 0 if does not exist.
*/
int existPair( VERTEX_UV_NORMAL_PAIR pair, GLfloat * vertexData, GLfloat * uvData, GLfloat * normalData, int bufferCount, int outputCount )
{

	// Buffers empty
	if ( outputCount == 0 ) 
	{
		return -1;
	}

	// para cada elemento no bufer de saida
	for( int i = 0; i < outputCount; i++ )
	{

		if( is_near( pair.vertex[0] , vertexData[ ( i * 3 ) ] ) && 
			is_near( pair.vertex[1] , vertexData[ ( i * 3 ) + 1 ] ) && 
			is_near( pair.vertex[2] , vertexData[ ( i * 3 ) + 2 ] ) &&

			is_near( pair.uv[0] , uvData[ ( i * 2 ) ] ) &&
			is_near( pair.uv[1] , uvData[ ( i * 2 ) + 1 ] ) &&

			is_near( pair.normal[0] , normalData[ ( i * 3 ) ] ) &&
			is_near( pair.normal[1] , normalData[ ( i * 3 ) + 1 ] ) &&
			is_near( pair.normal[2] , normalData[ ( i * 3 ) + 2 ] ) )
		{
			// Found same data in output buffers return its index
			return i;
		}
	}

	return -1;
}