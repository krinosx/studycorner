#include "shader.h"

#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <tchar.h>
#include <stdio.h>
#include <vector>
#include <algorithm>    // std::max

#define MAX_ERROR_MSG_SIZE 1024

Shader::Shader( const char * vertexShader, const char * pixelShader )
{
	this->vertexShaderName = vertexShader;
	this->pixelShaderName = pixelShader;
	
	this->shaderId = this->loadShaders();
}

Shader::~Shader()
{
}

GLuint Shader::getShaderId()
{
	return this->shaderId;
}

GLuint Shader::loadShaders(){

	const char * vertex_file_path = this->vertexShaderName;
	const char * fragment_file_path = this->pixelShaderName;


    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
 
    // Read the Vertex Shader code from the file

	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if(VertexShaderStream.is_open())
    {
        std::string Line = "";
        while(std::getline(VertexShaderStream, Line))
            VertexShaderCode += "\n" + Line;
        VertexShaderStream.close();
    }
 
    // Read the Fragment Shader code from the file
    std::string FragmentShaderCode;
    std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
    if(FragmentShaderStream.is_open()){
        std::string Line = "";
        while(getline(FragmentShaderStream, Line))
            FragmentShaderCode += "\n" + Line;
        FragmentShaderStream.close();
    }
 
    GLint Result = GL_FALSE;
    
    // Compile Vertex Shader
    printf("Compiling shader : %s\n", vertex_file_path);
    char const * VertexSourcePointer = VertexShaderCode.c_str();
    glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
    glCompileShader(VertexShaderID);
 
    // Check Vertex Shader
    char vertexShaderErrorMessage[MAX_ERROR_MSG_SIZE];
	int vertexErrorLenght = 0;

	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &vertexErrorLenght);
	
    
	glGetShaderInfoLog(VertexShaderID, MAX_ERROR_MSG_SIZE, &vertexErrorLenght, vertexShaderErrorMessage );
	
	if( vertexErrorLenght > 0 ) {
		fprintf(stdout, "%s\n", vertexShaderErrorMessage );
	}
 
	
    // Compile Fragment Shader
    printf("Compiling shader : %s\n", fragment_file_path);
    char const * FragmentSourcePointer = FragmentShaderCode.c_str();
    glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
    glCompileShader(FragmentShaderID);
 
    // Check Fragment Shader

	    // Check Vertex Shader
    char fragShaderErrorMessage[MAX_ERROR_MSG_SIZE];
	int fragErrorLenght = 0;

    glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &fragErrorLenght);
    
	glGetShaderInfoLog(FragmentShaderID, MAX_ERROR_MSG_SIZE, NULL, fragShaderErrorMessage );
	if( fragErrorLenght > 0 ){
		fprintf(stdout, "%s\n", fragShaderErrorMessage);
	}
 
    // Link the program
    fprintf(stdout, "Linking program\n");
    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, VertexShaderID);
    glAttachShader(ProgramID, FragmentShaderID);
    glLinkProgram(ProgramID);
 
    // Check the program
	char progShaderErrorMessage[MAX_ERROR_MSG_SIZE];
	int progErrorLenght = 0;

    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &progErrorLenght);
    
	glGetProgramInfoLog(ProgramID, MAX_ERROR_MSG_SIZE, NULL,progShaderErrorMessage );
	if( progErrorLenght > 0 ) {
		fprintf(stdout, "%s\n", progShaderErrorMessage );
	}
 
    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);
 
	fprintf(stdout, "Program Compiled. Id $d \n", ProgramID);
    return ProgramID;
}