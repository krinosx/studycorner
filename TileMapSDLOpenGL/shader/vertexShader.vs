#version 150
uniform mat4 modelViewProjection;
uniform mat4 model;

in vec3 vertexPosition_modelspace;
in vec3 normalData;
in vec2 vertexUV;

out vec3 fragVert;
out vec3 fragNormal;
out vec2 fragTexCoord;

void main()
{
	// Coordenadas homogeneas
	vec4 vertice = vec4( vertexPosition_modelspace , 1 );

	gl_Position = modelViewProjection * vertice;
	
	fragVert = vertexPosition_modelspace;
	fragNormal = normalData;
	fragTexCoord = vertexUV;
}