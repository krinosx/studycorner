#include <windows.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include <stdio.h>
#include "texture.h"
#include "OpenGLWindow.h"
#include "Model.h"
#include "ModelLoader.h"
#include <SDL.h>


int main(int argc, char **argv)
{	


	OpenGLWindow *window = new OpenGLWindow("SDL + OpenGL", 800, 600);
	
	window->setListener(window);
	
	// Shadeder depends on glewInit and glew depends on OpenGLWindow
	// So must move loadShader function to window
	Shader *basicShader = new Shader("shader/vertexShader.vs","shader/pixelShader.ps");
	// Load textures
	Texture *tex = new Texture("texture/textura.bmp");
	Texture *texPoste = new Texture("texture/Poste.bmp");
	
	// Create a 'camera object'
	Camera myCamera = Camera();
	myCamera.setCameraUpAxis(0.0f,1.0f,0.0f);
	myCamera.setCameraPosition(0.0f, 3.0f, 4.0f);
	myCamera.setCameraDirection(0.0f, 0.0f, -1.0f );
	myCamera.setCameraFrustrum(45.0f, 4.0f / 3.0f, 0.1f, 100.0f);
	
	window->setCamera( myCamera );

	
	/*
	 * Draw a cube with VBO indexed way
	 */

	
	ModelLoader modelLoader;

	Model *model = modelLoader.loadModel( "objs/StrangeAsset.obj" );
	
	VertexData modelData;
	model->getVertexBufferData( &modelData );

	VisualObject * voIndexed = new VisualObject();
	voIndexed->setVertexData( modelData.vertexBufferData, modelData.vertexBufferSize, modelData.vertexCount );
	voIndexed->setIndexData( modelData.indexBufferData, modelData.indexSize, modelData.indexCount );
	voIndexed->setUvData( modelData.uvBufferData, modelData.uvBufferSize, modelData.uvCount );
	voIndexed->setNormalData(modelData.normalBufferData, modelData.normalBufferSize, modelData.normalCount);

	voIndexed->setTexture( tex );
	voIndexed->setShader( basicShader );
	voIndexed->setPosition( 6.0f, 0.0f, -4.0f );
	voIndexed->setRotation( 0.0f, 1.0f, 0.0f );
	voIndexed->setScale( 1.0f );
		
	window->addObject( *voIndexed );
		
	Model *modelPoste = modelLoader.loadModel( "objs/Poste.obj" );
	
	modelPoste->getVertexBufferData( &modelData );

	VisualObject * posteVo = new VisualObject();
	posteVo->setVertexData( modelData.vertexBufferData, modelData.vertexBufferSize, modelData.vertexCount );
	posteVo->setIndexData( modelData.indexBufferData, modelData.indexSize, modelData.indexCount );
	posteVo->setUvData( modelData.uvBufferData, modelData.uvBufferSize, modelData.uvCount );
	posteVo->setNormalData(modelData.normalBufferData, modelData.normalBufferSize, modelData.normalCount);

	posteVo->setTexture( texPoste );
	posteVo->setShader( basicShader );
	posteVo->setPosition( 6.0f, 0.0f, -4.0f );
	posteVo->setRotation( 0.0f, 1.0f, 0.0f );
	posteVo->setScale( 1.0f );

	window->addObject( *posteVo );

	


	/*
	 * Draw a cube with VBO indexed way
	 */

	window->startLoop();



	return 0;
}