#ifndef VISUALOBJECT_H
#define VISUALOBJECT_H
#define GLM_FORCE_RADIANS

#include "shader.h"
#include <gl\glew.h>
#include <gl\gl.h>
#include <glm.hpp>
#include "camera.h"
#include "texture.h"

class VisualObject
{
private:
	GLuint vertexArrayId;
	
	/*  Vertex Information */
	GLuint vertexbufferId;
	const GLfloat * g_vertex_buffer_data;
	long vertex_buffer_size;
	long vertex_count;

	/*Normals Information */
	GLuint normalbufferId;
	const GLfloat * g_normal_buffer_data;
	long normal_buffer_size;
	long normal_count;
	

	/*  Vertex Index Information */
	GLuint   indexBufferId;
	GLuint * indexBufferData;
	long   indexBufferSize;
	long   indexBufferCount;
	
	
	GLuint uvbufferId;
	Texture *texture;	
	GLuint shaderId;
	
	static long currentId;
	/* Object position */
	glm::vec3 position;
	glm::mat4 scale;
	glm::vec3 rotation;
	
public:
	VisualObject();
	~VisualObject();
	void setShader(Shader * basicShader);
	void setVertexData(const GLfloat *vertexData, long bufferSize, long vertexCount);
	void setIndexData(GLuint * indexData, long bufferSize, long indexCount);
	void setUvData(const GLfloat *uvData, long bufferSize, long vertexCount);
	void setNormalData(const GLfloat *normalData, long bufferSize, long normalCount);
	
	void draw(Camera camera);
	void setPosition( float x, float y, float z );
	void setScale( float scaleFactor );
	void setRotation( float rX, float rY, float rZ );
	glm::mat4 getModelMatrix();
	void setTexture( Texture *texture );

	void debugData();
};

#endif // VISUALOBJECT_H