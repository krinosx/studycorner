#ifndef INPUTHANDLER_H
#define INPUTHANDLER_H
#include <iostream>
#include <sdl.h>

class InputHandler
{
	public:
		InputHandler(){};
		virtual ~InputHandler(){};
		virtual void keyPressed( SDL_Keycode keycode ){ std::cout << "pressed: " << keycode << "\r\n"; };
		virtual void mousePressed( int x, int y ){};
};

#endif // INPUTHANDLER_H