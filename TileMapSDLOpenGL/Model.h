#ifndef MODEL_H
#define MODEL_H

#include <gl\glew.h>
#include <gl\gl.h>
#include <iostream>

struct VERTEX_DATA {
  GLfloat * vertexBufferData;
  GLint vertexBufferSize;
  GLint vertexCount;

  GLfloat * uvBufferData;
  GLint uvBufferSize;
  GLint uvCount;

  GLfloat * normalBufferData;
  GLint normalBufferSize;
  GLint normalCount;

  GLuint * indexBufferData;
  GLint indexSize;
  GLint indexCount;

};

typedef VERTEX_DATA VertexData;

class Model
{
private:
	std::string modelName;

	GLfloat * vertexBufferData;
	GLint vertexBufferSize;
	GLint vertexBufferCount;
	
	GLuint * vertexBufferIndexesData;
	GLint vertexBufferIndexesSize;
	GLint vertexBufferIndexesCount;

	GLfloat * uvBufferData;
	GLint uvBufferSize;
	GLint uvBufferCount;

	
	GLfloat * normalBufferData;
	GLint normalBufferSize;
	GLint normalBufferCount;


public:
	Model(void);
	~Model(void);

	void setVertexData( GLfloat * bufferData, GLint bufferSize, GLint bufferCount );
	void setVertexIndexData( GLuint * bufferData, GLint bufferSize, GLint bufferCount );
	void setUvData( GLfloat * bufferData, GLint bufferSize, GLint bufferCount );
	void setNormalData( GLfloat * bufferData, GLint bufferSize, GLint bufferCount);
	void getVertexBufferData( VertexData * vertexData );

};

#endif // #ifndef MODEL_H
