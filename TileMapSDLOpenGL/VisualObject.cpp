#include <stdio.h>
#include "VisualObject.h"
#include "camera.h"
#include <glm.hpp>
#include <gtc/matrix_transform.hpp> 
#include <gtx/transform.hpp>

long VisualObject::currentId = 0;

VisualObject::VisualObject()
{
	// Starting object
	// Generata a VertexArray for current object
	glGenVertexArrays(1,&this->vertexArrayId);
	
	this->texture = NULL;
	this->uvbufferId = 0;
	
	this->normalbufferId = 0;
	this->g_normal_buffer_data = NULL;
	this->normal_buffer_size = 0;
	this->normal_count = 0;

	this->indexBufferId = 0;
	this->indexBufferCount = 0;
	this->indexBufferData = NULL;
}


void VisualObject::draw(Camera camera)
{
	glm::mat4 modelMatrix = this->getModelMatrix();
	glm::mat4 projection = camera.getModelViewProjectionMatrix( modelMatrix );
	
	// use curent shader
	glUseProgram( this->shaderId );	
	
	// set the uniform MVP
	GLuint mvpPosition = glGetUniformLocation(this->shaderId,"modelViewProjection");
	glUniformMatrix4fv(mvpPosition, 1, GL_FALSE, &projection[0][0]);

	GLuint modelPosition = glGetUniformLocation(this->shaderId, "model");
	glUniformMatrix4fv(modelPosition, 1, GL_FALSE, &modelMatrix[0][0]);
	

	GLint vertexPosition = glGetAttribLocation(this->shaderId, "vertexPosition_modelspace");
	GLint normalPosition = glGetAttribLocation(this->shaderId, "normalData");
	GLint uvCoordinates = glGetAttribLocation(this->shaderId, "vertexUV");

	
	// Enable vertex attrib 1 - Vertex Data
	glEnableVertexAttribArray(vertexPosition);
		glBindBuffer(GL_ARRAY_BUFFER, this->vertexbufferId);
		glVertexAttribPointer(
			vertexPosition,	// Position on vertex shadder
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			(void*)0
		);
		
		glEnableVertexAttribArray(normalPosition);
		glBindBuffer(GL_ARRAY_BUFFER, this->normalbufferId);
		glVertexAttribPointer(
				normalPosition,
				3,
				GL_FLOAT,
				GL_FALSE,
				0,
				(void*)0
			);
	
	if( this->texture != NULL )	
	{
		// Enable vertex attrib 2 - UV data
		glEnableVertexAttribArray(uvCoordinates);
		glBindBuffer(GL_ARRAY_BUFFER, this->uvbufferId);
		glVertexAttribPointer(
				uvCoordinates,                                // attribute. No particular reason for 1, but must match the layout in the shader.
				2,                                // size : U+V => 2
				GL_FLOAT,                         // type
				GL_FALSE,                         // normalized?
				0,                                // stride
				(void*)0                          // array buffer offset
			);

		// Activate the texture
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, this->texture->getTextureId() );
		glUniform1i( this->texture->getTextureId(), 0);
	}		

	

	if( this->indexBufferId > 0 )
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->indexBufferId );
		glDrawElements( GL_TRIANGLES, this->indexBufferCount, GL_UNSIGNED_INT, (void*)0 );
	}
	else
	{
		glDrawArrays(GL_TRIANGLES,0,this->vertex_count);
	}
	
	glDisableVertexAttribArray(vertexPosition);
	glDisableVertexAttribArray(normalPosition);
	
	if( this->texture != NULL )
	{
		glDisableVertexAttribArray(uvCoordinates);
	}
}

void VisualObject::setPosition( float x, float y, float z )
{
	this->position = glm::vec3(x,y,z);
}
void VisualObject::setScale( float scaleFactor )
{
		this->scale = glm::scale( glm::mat4(1.0f), glm::vec3(scaleFactor));
		
		
}
void VisualObject::setRotation( float rX, float rY, float rZ )
{
	this->rotation = glm::vec3( rX, rY, rZ );
}


glm::mat4 VisualObject::getModelMatrix()
{
	glm::mat4 rotationMatrix = glm::rotate( 0.0f, this->rotation );
	glm::mat4 scaleMatrix = this->scale;
	glm::mat4 positionMatrix = glm::translate(glm::mat4(1.0f), this->position);
	
	return positionMatrix * rotationMatrix * scaleMatrix;
}


void VisualObject::setShader(Shader * basicShader)
{
	this->shaderId = basicShader->getShaderId();
}

void VisualObject::setVertexData(const GLfloat * vertexData, long bufferSize, long vertexCount )
{
	this->vertex_count = vertexCount;
	// Enable object vertexArray to bind the buffers
	glBindVertexArray( this->vertexArrayId );	
	
	this->g_vertex_buffer_data = vertexData;
	this->vertex_buffer_size = bufferSize;
	
	glGenBuffers(1, &this->vertexbufferId);
	glBindBuffer(GL_ARRAY_BUFFER, this->vertexbufferId);
	glBufferData(GL_ARRAY_BUFFER, this->vertex_buffer_size, this->g_vertex_buffer_data, GL_STATIC_DRAW);
}


void VisualObject::setNormalData(const GLfloat *normalData, long bufferSize, long normalCount)
{

	this->normal_count = normalCount;
	this->g_normal_buffer_data = normalData;
	this->normal_buffer_size = bufferSize;


	glBindVertexArray(this->vertexArrayId);
	glGenBuffers(1, &this->normalbufferId);
	glBindBuffer(GL_ARRAY_BUFFER, this->normalbufferId);
	glBufferData(GL_ARRAY_BUFFER, this->normal_buffer_size, this->g_normal_buffer_data, GL_STATIC_DRAW);
}

void VisualObject::setUvData(const GLfloat * uvData, long bufferSize, long vertexCount)
{
	// Enable object vertexArray to bind the buffers
	glBindVertexArray( this->vertexArrayId );	

	glGenBuffers(1, &this->uvbufferId);
	glBindBuffer(GL_ARRAY_BUFFER, this->uvbufferId);
	glBufferData(GL_ARRAY_BUFFER, bufferSize, uvData, GL_STATIC_DRAW);
}


void VisualObject::setIndexData( GLuint * indexData, long bufferSize, long indexCount )
{
	// Enable object vertexArray to bind the buffers
	glBindVertexArray( this->vertexArrayId );	

	this->indexBufferCount = indexCount;
	this->indexBufferSize = bufferSize;
	this->indexBufferData = indexData;

	glGenBuffers(1, &this->indexBufferId );
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->indexBufferId );
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indexBufferSize, this->indexBufferData, GL_STATIC_DRAW );

}

void VisualObject::setTexture( Texture *texture )
{
	this->texture = texture;
}


VisualObject::~VisualObject()
{
	// TODO: Unload all buffers
}


void VisualObject::debugData()
{

	// Print buffer data
	int vertexcount = 0;
	for( int i = 0; i < this->vertex_count; i = i+3 )
	{
		printf( "Vertex [ %f %f %f ] - v%d \r\n ", 
			this->g_vertex_buffer_data[i],
			this->g_vertex_buffer_data[i+1],
			this->g_vertex_buffer_data[i+2],
			vertexcount++
		);
	}
	printf("Total Vertex Count: %d \r\n", this->vertex_count );
	
	int indexCount = 0;
	for( int j = 0; j < this->indexBufferCount; j = j+3 )
	{
		printf( "Index [ %d %d %d ]  f%d \r\n ",
			this->indexBufferData[j],
			this->indexBufferData[j+1],
			this->indexBufferData[j+2],
			indexCount++
			);
	}
	printf( "Total index elements: %d \r\n", indexBufferCount );

}