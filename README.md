#**TileMapSDLOpenGL Project Overview** #

	Objective:
		This application is been build to become a client to a 'client serve
		game'. Its primary objective will be to render the maps hosted by 
		the	server and collect commands from our users to send and be 
		processed by the server.

	Used Technologies
		- OpenGL: For 3d rendering
		- SDL: To handle window, input and image loading
		- glm: To handle 3d math ( like vec3, mat4 operations )
		- glew: to load opengl extensions
		- ** Some lib to be added ** : to handle network features
	
	New Features ( next steps )
		* Add a texture manager: Create a class to handle texture 
		requisitions, hold already loaded textures and deal with loading 
		image code ( remove this logic from Texture.cpp class. It will be 
		usefull when various models will share the same texture )
		* Create a Shadder manager: The same principle as TextureManager
		* Add Illumination features: Develop some shaders to deal with 
		simple lights and/or dynamic lights.
		* Add support for multiple textures in the same object ( bump map, 
		light map, etc )
		* Implement a Pick Algorithm: Enable user to select objects with 
		mouse clicks.
		* Implement networking : The application must be able to connect
		to a remote server ( already implemented ) and send/receive text
		commands.
		* Implement MapLoading : This application will receive a list of
		tile IDs from our server and must be able to load the tiles and
		render them on the screen according with server command received.
		* Send Commands : Based on user clicks ( mouse interaction ) and
		keyboard interaction this application must be able to generate 
		commands and set it to connected server.
	
	========================================================================
	 Progress
		22/02/2014:
			Untill now the application have:
			* A simple OpenGL setup with some camera controls
			* A loading model ( ModelLoader.cpp/.h ) to load .obj files
			* A Texture.cpp/.h to deal with texture loading instancing
			* A Shadder.cpp/.h to deal with shadder compiling
			* A visual object class to abstract the draw logic

		TODO List ( improvements )
			* Texture.cpp :  Remove custom loading image logic and
			use SDL lib to load texture images
			* ModelLoader.cpp : Optimize Vertex/UV indexing to avoid 
			vertex/uv duplicity and to support loading of normal 
			attributes from the .obj file
			* Camera.cpp : Add some logic to move the camera ( strafe 
			with Q and E keys )
			* VisualObject.cpp : Remove the VAO binding from the VisualObject,
			it must be bound not by object, but by Shader ( research must be 
			made to ensure this bind logic )
